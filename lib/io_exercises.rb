# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
answer = rand(1..100)
#p answer
won = false
guesses = []
num_guesses = 0

  until won
  puts "Guess a number between 1 and 100:"
  guess = gets.chomp.to_i
  puts "your guess: #{guess}"

    if guess == answer
    num_guesses += 1
    won = true
    elsif guess > 100 || guess < 0
    puts "please input a number between 1 and 100"
    elsif guess > answer && !guesses.include?(guess)
    puts "Your guess is too high."
    num_guesses += 1
    puts "Number of guesses so far: #{num_guesses}"
    guesses << guess
    elsif guess < answer && !guesses.include?(guess)
    puts "Your guess is too low."
    num_guesses += 1
    puts "Number of guesses so far: #{num_guesses}"
    guesses << guess
    elsif guesses.include?(guess)
    puts "You have already guessed #{guess}"
    puts "guesses: #{guesses.join(",")}"

    end

  end


  if won
    puts "Number of guesses so far: #{num_guesses}"
    puts "#{guess} is the correct answer! You have won!"
  end

end
